#ifndef _CALCVOLUME_H_
#define _CALCVOLUME_H_

#include "volume.h"

void calcVolumePiramide();
void calcVolumeCubo();
void calcVolumeParalelepipedo();
void calcVolumeEsfera();

#endif