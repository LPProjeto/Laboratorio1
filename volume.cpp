#include <cmath>
#include "volume.h"

float VolumePiramide(float arestaBase, float altura)
{
	float Volume;

	Volume = (1/3)*(arestaBase*arestaBase)*altura;

	return Volume;
}

float VolumeCubo(float aresta)
{
	float Volume;

	Volume = pow(aresta,3);

	return Volume;
}

float VolumeParalelepipedo(float aresta1, float aresta2, float aresta3)
{
	float Volume;

	Volume = aresta1*aresta2*aresta3;

	return Volume;
}

float VolumeEsfera(float raio)
{
	float Volume;

	Volume = (4/3)*(PI)*(pow(raio, 3));

	return Volume;
}