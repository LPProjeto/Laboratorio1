#include <iostream>
#include "calcarea.h"
#include "area.h"


using namespace std;
//Funções que devem ser chamadas pelo main.cpp
//Funções que solicitam dados ao usuario e chama a função que realizara o calculo da Area.

void calcAreaTriangulo()//Deve chamar essa função quando o usuario escolher a -opção 1-;
{
	cout << "Digite o tamanho da base do triângulo: ";
	float baseTriangulo;
	cin >> baseTriangulo;

	cout << "Digite o tamanho da altura do triangulo: ";
	float alturaTriangulo;
	cin >> alturaTriangulo;

	//Chamar função para realizar calculo
	float Area;
	Area = AreaTriangulo(baseTriangulo, alturaTriangulo);

	cout << "Area do triangulo: " << Area << endl;
}

void calcAreaRetangulo() //Deve chamar essa função quando o usuario escolher a -opção 2-;
{
	cout << "Digite o tamanho da base do retangulo: ";
	float baseRetangulo;
	cin >> baseRetangulo;

	cout << "Digite o tamanho da altura do retangulo: ";
	float alturaRetangulo;
	cin >> alturaRetangulo;

	float Area;
	Area = AreaRetangulo(baseRetangulo, alturaRetangulo);

	cout << "Area do retangulo: "<< Area << endl;
}

void calcAreaQuadrado()//Deve chamar essa função quando o usuario escolher a -opção 3-;
{
	cout << "Digite o tamanho do lado do quadrado: ";
	float ladoQuadrado;
	cin >> ladoQuadrado;

	float Area;
	Area = AreaQuadrado(ladoQuadrado);

	cout << "Area do quadrado: "<< Area << endl;
}

void calcAreaCirculo()//Deve chamar essa função quando o usuario escolher a -opção 4-;
{
	cout << "Digite o tamanho do raio do circulo: ";
	float raioCirculo;
	cin >> raioCirculo;

	float Area;
	Area = AreaCirculo(raioCirculo);

	cout << "Area do circulo: "<< Area << endl;
}

void calcAreaPiramide() //Deve chamar essa função quando o usuario escolher a -opção 5-;
{
	cout << "Digite o tamanho da base da piramide: ";
	float basePiramide;
	cin >> basePiramide;

	cout << "Digite o tamanho da altura da piramide: ";
	float alturaPiramide;
	cin >> alturaPiramide;

	float Area;
	Area = AreaPiramide(basePiramide, alturaPiramide);

	cout << "Area da piramide: "<< Area << endl;
}

void calcAreaCubo() //Deve chamar essa função quando o usuario escolher a -opção 6-;
{
	cout << "Digite o tamanho da aresta do cubo: ";
	float arestaCubo;
	cin >> arestaCubo;

	float Area;
	Area = AreaCubo(arestaCubo);

	cout << "Area do cubo: "<< Area << endl;
}

void calcAreaParalelepipedo() //Deve chamar essa função quando o usuario escolher a -opção 7-;
{
	cout << "Digite o tamanho da primeira aresta do paralelepipedo: ";
	float aresta1Paralelepipedo;
	cin >> aresta1Paralelepipedo;

	cout << "Digite o tamanho da segunda aresta do paralelepipedo: ";
	float aresta2Paralelepipedo;
	cin >> aresta2Paralelepipedo;

	cout << "Digite o tamanho da terceria aresta do paralelepipedo: ";
	float aresta3Paralelepipedo;
	cin >> aresta3Paralelepipedo;

	float Area;
	Area = AreaParalelepipedo(aresta1Paralelepipedo, aresta2Paralelepipedo, aresta3Paralelepipedo);

	cout << "Area do paralelepipedo: "<< Area << endl;
}

void calcAreaEsfera() //Deve chamar essa função quando o usuario escolher a -opção 8-;
{
	cout << "Digite o tamanho do raio da esfera: ";
	float raioEsfera;
	cin >> raioEsfera;

	float Area;
	Area = AreaEsfera(raioEsfera);

	cout << "Area da esfera: "<< Area << endl;
}