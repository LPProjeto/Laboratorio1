#ifndef _CALCPERIMETRO_H_
#define _CALCPERIMETRO_H_

#include "perimetro.h"

void calcPerimetroTriangulo();
void calcPerimetroRetangulo();
void calcPerimetroQuadrado();
void calcPerimetroCirculo();

#endif