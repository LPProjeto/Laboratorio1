#include <iostream>
#include "calcarea.h"
#include "calcperimetro.h"
#include "calcvolume.h"

using namespace std;

void MenuOpcoes();

int main()
{
	int Opcao = 0;

	do
	{
		//Menu de seleção
		MenuOpcoes();

		cin >> Opcao;
		
		switch(Opcao){
			case 1:
				calcAreaTriangulo();
				calcPerimetroTriangulo();
				break;
			case 2:
				calcAreaRetangulo();
				calcPerimetroRetangulo();
				break;
			case 3:
				calcAreaQuadrado();
				calcPerimetroQuadrado();
				break;
			case 4:
				calcAreaCirculo();
				calcPerimetroCirculo();
				break;
			case 5:
				calcAreaPiramide();
				calcVolumePiramide();
				break;
			case 6:
				calcAreaCubo();
				calcVolumeCubo();
				break;
			case 7:
				calcAreaParalelepipedo();
				calcVolumeParalelepipedo();
				break;
			case 8:
				calcAreaEsfera();
				calcVolumeEsfera();
				break;
			case 0:
				break;
			default:
				cout << "Opcao invalida!" << endl;
				break;
		}


	} while(Opcao != 0);

	return 0;
}

void MenuOpcoes()
{
	cout << "Calculadora de Geometria Plana e Espacial" << endl;
	cout << "(1) Triangulo equilatero" << endl;
	cout << "(2) Retangulo" << endl;
	cout << "(3) Quadrado" << endl;
	cout << "(4) Circulo" << endl;
	cout << "(5) Piramide com base quadrangular" << endl;
	cout << "(6) Cubo" << endl;
	cout << "(7) Paralelepipedo" << endl;
	cout << "(8) Esfera" << endl;
	cout << "(0) Sair" << endl;
	cout << endl;
	cout << "Digite a sua opcao: ";
}