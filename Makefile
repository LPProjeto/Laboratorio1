#Makefilefor "imd0030" C++ application

#Para utilizar no windows com mingw32 ir ate o diretorio que contem o makefile pelo prompt e utilizar o comando
# >mingw32-make.exe
#No linux basta utilizar e no diretorio e utilizar o comando
# >make

PROG = geometrica
CC = g++
CPPFLAGS = -O0 -g -Wall -pedantic
OBJS = main.o calcvolume.o volume.o calcperimetro.o perimetro.o calcarea.o area.o

$(PROG):$(OBJS)
	$(CC) -o $(PROG) $(OBJS)

main.o:
	$(CC) $(CPPFLAGS) -c main.cpp

calcvolume.o: calcvolume.h
	$(CC) $(CPPFLAGS) -c calcvolume.cpp

volume.o: volume.h
	$(CC) $(CPPFLAGS) -c volume.cpp

calcperimetro.o: calcperimetro.h
	$(CC) $(CPPFLAGS) -c calcperimetro.cpp

perimetro.o: perimetro.h
	$(CC) $(CPPFLAGS) -c perimetro.cpp

calcarea.o: calcarea.h
	$(CC) $(CPPFLAGS) -c calcarea.cpp

area.o: area.h
	$(CC) $(CPPFLAGS) -c area.cpp

clean:
	rm -f core $(PROG) $(OBJS)
