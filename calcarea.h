#ifndef _CALCAREA_H_
#define _CALCAREA_H_

void calcAreaTriangulo();
void calcAreaRetangulo();
void calcAreaQuadrado();
void calcAreaCirculo();
void calcAreaPiramide();
void calcAreaCubo();
void calcAreaParalelepipedo();
void calcAreaEsfera();

#endif