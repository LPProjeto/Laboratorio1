#ifndef _PERIMETRO_H_
#define _PERIMETRO_H_

#include "perimetro.h"

#define PI 3.1415

float PerimetroTriangulo(float lado);
float PerimetroRetangulo(float base, float altura);
float PerimetroQuadrado(float lado);
float PerimetroCirculo(float raio);

#endif