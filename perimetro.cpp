#include "perimetro.h"

float PerimetroTriangulo(float lado)
{
	float Perimetro;

	Perimetro = 3*lado;

	return Perimetro;
}

float PerimetroRetangulo(float base, float altura)
{
	float Perimetro;

	Perimetro = 2*(base+altura);

	return Perimetro;
}

float PerimetroQuadrado(float lado)
{
	float Perimetro;

	Perimetro = 4*lado;

	return Perimetro;
}

float PerimetroCirculo(float raio)
{
	float Perimetro;

	Perimetro = 2*PI*raio;

	return Perimetro;
}