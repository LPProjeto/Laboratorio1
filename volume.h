#ifndef _VOLUME_H_
#define _VOLUME_H_

#include "volume.h"

#define PI 3.1415

float VolumePiramide(float arestaBase, float altura);
float VolumeCubo(float aresta);
float VolumeParalelepipedo(float aresta1, float aresta2, float aresta3);
float VolumeEsfera(float raio);

#endif