#include "area.h"
#include <cmath>

float AreaTriangulo(float base, float altura)
{
	float Area;

	Area = (base*altura)/2;

	return Area;
}

float AreaRetangulo(float base, float altura)
{
	float Area;

	Area = base*altura;

	return Area;
}

float AreaQuadrado(float lado)
{
	float Area;

	Area = pow(lado, 2);

	return Area;
}

float AreaCirculo(float raio)
{
	float Area;

	Area = PI*(pow(raio, 2));

	return Area;
}

float AreaPiramide(float base, float altura)
{
	//Calcular area da base
	float AreaBase;
	AreaBase = base*base;

	//Calcular altura lateral
	float AlturaLateral;
	AlturaLateral = sqrt(pow((base/2),2)+ pow(altura,2));
	
	//Calcular area lateral
	float AreaLateral;
	AreaLateral = AreaTriangulo(base, AlturaLateral);

	//Calcular Area da Piramide
	float Area;
	Area = AreaBase + 6*AreaLateral;

	return Area; 
}

float AreaCubo(float aresta)
{
	//Calcular area da base
	float AreaBase;
	AreaBase = aresta*aresta;

	//Calcular area do cubo
	float Area;
	Area = AreaBase*6;

	return Area;
}

float AreaParalelepipedo(float aresta1, float aresta2, float aresta3)
{
	float Area;

	Area = (2*aresta1*aresta2) + (2*aresta1*aresta3) + (2*aresta2*aresta3);

	return Area;
}

float AreaEsfera(float raio)
{
	float Area;

	Area = 4*AreaCirculo(raio);

	return Area;
}