#include <iostream>
#include "calcvolume.h"
#include "volume.h"


using namespace std;
//Funções que devem ser chamadas pelo main.cpp
//Funções que solicitam dados ao usuario e chama a função que realizara o calculo do Volume

void calcVolumePiramide()
{
	cout << "Digite o tamanho da aresta da base da piramide: ";
	float arestaBase;
	cin >> arestaBase;

	cout << "Digite a altura da piramide: ";
	float altura;
	cin >> altura;

	float Volume;
	Volume = VolumePiramide(arestaBase, altura);

	cout << "Volume da piramide: " << Volume << endl;
}

void calcVolumeCubo()
{
	cout << "Digite o tamanho de uma aresta do cubo: ";
	float aresta;
	cin >> aresta;

	float Volume;
	Volume = VolumeCubo(aresta);

	cout << "Volume do cubo: " << Volume << endl;
}

void calcVolumeParalelepipedo()//Deve chamar essa função quando o usuario escolher a -opção 7-;
{
	cout << "Digite o tamanho da primeira aresta do paralelepipedo: ";
	float aresta1Paralelepipedo;
	cin >> aresta1Paralelepipedo;

	cout << "Digite o tamanho da segunda aresta do paralelepipedo: ";
	float aresta2Paralelepipedo;
	cin >> aresta2Paralelepipedo;

	cout << "Digite o tamanho da terceria aresta do paralelepipedo: ";
	float aresta3Paralelepipedo;
	cin >> aresta3Paralelepipedo;

	float Volume;
	Volume = VolumeParalelepipedo(aresta1Paralelepipedo, aresta2Paralelepipedo, aresta3Paralelepipedo);

	cout << "Volume do paralelepipedo: "<< Volume << endl;
}

void calcVolumeEsfera()//Deve chamar essa função quando o usuario escolher a -opção 8-;
{
	cout << "Digite o tamanho do raio da esfera: ";
	float raioEsfera;
	cin >> raioEsfera;
	
	float Volume;
	Volume = VolumeEsfera(raioEsfera);

	cout << "Volume da esfera "<< Volume << endl;
}