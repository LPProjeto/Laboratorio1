#include <iostream>
#include "calcperimetro.h"
#include "perimetro.h"


using namespace std;
//Funções que devem ser chamadas pelo main.cpp
//Funções que solicitam dados ao usuario e chama a função que realizara o calculo do Perimetro

void calcPerimetroTriangulo()
{
	cout << "Digite o tamanho do lado do triangulo: ";
	float ladoTriangulo;
	cin >> ladoTriangulo;

	float Perimetro;
	Perimetro = PerimetroTriangulo(ladoTriangulo);

	cout << "Perimetro do triangulo: "<< Perimetro << endl;	
}

void calcPerimetroRetangulo()
{
	cout << "Digite o tamanho da base do retangulo: ";
	float baseRetangulo;
	cin >> baseRetangulo;

	cout << "Digite o tamanho da altura do retangulo: ";
	float alturaRetangulo;
	cin >> alturaRetangulo;

	float Perimetro;
	Perimetro = PerimetroRetangulo(baseRetangulo, alturaRetangulo);

	cout << "Perimetro do triangulo: "<< Perimetro << endl;	
}

void calcPerimetroQuadrado()//Deve chamar essa função quando o usuario escolher a -opção 3-;
{
	cout << "Digite o tamanho do lado do quadrado: ";
	float ladoQuadrado;
	cin >> ladoQuadrado;

	float Perimetro;
	Perimetro = PerimetroQuadrado(ladoQuadrado);

	cout << "Perimetro do quadrado: "<< Perimetro << endl;
}

void calcPerimetroCirculo()//Deve chamar essa função quando o usuario escolher a -opção 4-;
{
	cout << "Digite o tamanho do raio do circulo: ";
	float raioCirculo;
	cin >> raioCirculo;

	float Perimetro;
	Perimetro = PerimetroCirculo(raioCirculo);

	cout << "Perimetro do quadrado: "<< Perimetro << endl;
}