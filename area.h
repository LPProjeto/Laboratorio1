#ifndef _AREA_H_
#define _AREA_H_

#define PI 3.1415

float AreaTriangulo(float base, float altura);
float AreaRetangulo(float base, float altura);
float AreaQuadrado(float lado);
float AreaCirculo(float raio);
float AreaPiramide(float base, float altura);
float AreaCubo(float aresta);
float AreaParalelepipedo(float aresta1, float aresta2, float aresta3);
float AreaEsfera(float raio);

#endif